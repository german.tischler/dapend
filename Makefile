BINARIES=daparse

all: $(BINARIES)

daparse: daparse.cpp
	${CXX} -W -Wall -O3 -g -o $@ $<
	
clean:
	rm -f *~

distclean: clean
	rm -f $(BINARIES)

DESTDIR ?=
PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
INCLUDEDIR ?= $(PREFIX)/include
LIBDIR ?= $(PREFIX)/lib

install: $(BINARIES)
	mkdir -p $(DESTDIR)$(BINDIR)
	cp $(BINARIES) $(DESTDIR)$(BINDIR)
