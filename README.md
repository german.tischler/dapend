# dapend

dapend is a language for encoding dependency information in a simple concise
form.

The source code in this project is distributed under a BSD style license.

The project contains a program daparse which is an example of a parser for
dapend. It parses a dapend file and produces a Unix make file from it.
This can then be execute locally using the make tool or on a cluster using
e.g. hpcsched [https://gitlab.com/german.tischler/hpcsched].

# Building the parser

The parser can be built in a Unix type shell using

```
make
```

# Installing the parser

The make file provided has an install target. The installation path can be
given via the PREFIX variable, e.g.:

```
PREFIX=${HOME}/dapend make install
```

This will install the daparse binary at ${HOME}/dapend/bin/daparse.

# The dapend file format

A depend file is similar to a Unix make file but allowing a for loop
construct in targets and dependencies to simplify and shorten repetetive
parts of the rule set.

In essence the file contains a sequence of rules. Each rule is structured
as

```
target: dependency1 dependency2 ...
	statement1
	statement2
	...
```

We have a rule producing target. It requires dependency1, dependency2 etc.
to run. target is produced by executing statement1, statement2 etc. in the
given order.

In addition the file can contain variable assignments, e.g.

```
A=3
```

comment lines (which start by the # symbol) e.g.

```
# This is a comment
```

and inline statements like

```
@echo "hello" > hello.txt
```

Variable A can be referenced using ${A} (note that the parentheses are
required).

Inline statements are special in the sense that these lines are executed
during the parser run using the set of currently active variables. The
set of currently active variables is defined by a top down scan of the
dapend file.

In each rule the target and dependency can be stated as exactly one
for loop using the syntax

```
for my_target.${i} in 1 .. 5
```

As an example consider the following rule:

```
for my_target.${i} in 1 .. 3: dependency.${i}
	ls directory_${i} > output_${i}
```

Stating this for loop rule is equivalent to writing the rules

```
my_target.1: dependency.1
	ls directory_1 > output_1
my_target.2: dependency.2
	ls directory_1 > output_1
my_target.3: dependency.3
	ls directory_1 > output_1
```

Similarly a rule dependency can be stated using a for loop, e.g.:

```
my_target: for dep_${i} in 1..4
	statement
```

is equivalent to

```
my_target: dep_1 dep_2 dep_3 dep_4
	statement
```

Note that the counting substitution in a for loop always
replaces the last variable in the string given, e.g. in

```
for rule_${i}_${j} in 1 .. 3
```

the substitution generate rule_${i}_1, rule_${i}_2 and rule_${i}_3.

One final extension of dapend relative to make files is that
rules can be nested. An example for this is

```
outer_rule:
	for mid_rule_${i} in 1 .. 3:
		for inner_rule_${i}_${j} in 1 .. 2:
			echo ${i} ${j}
```

This is equivalent to:

```
inner_rule_1_1:
	echo 1 1
inner_rule_1_2:
	echo 1 2
mid_rule_1: inner_rule_1_1 inner_rule_1_2
inner_rule_2_1:
	echo 2 1
inner_rule_2_2:
	echo 2 2
mid_rule_2: inner_rule_2_1 inner_rule_2_2
inner_rule_3_1:
	echo 3 1
inner_rule_3_2:
	echo 3 2
mid_rule_3: inner_rule_3_1 inner_rule_3_2
outer_rule: mid_rule_1 mid_rule_2 mid_rule_3
```

# Running the parser

The parser reads a dapend file on the standard input channel and produces
a make file on the standard output channel. It also produces a parsed
representation on the standard error channel showing all the variable
substituions and loop unrolling which have taken place. An example run is

```
daparse < input.dapend > input.mk
```

# Example dapend file for daligner

The following short dapend file shows how to define rules for a daligner
[https://github.com/thegenemyers/DALIGNER] run:

```
#
# sample dapend file showing daligner dependencies
#
# we first produce all pairwise block alignments
# then we merge the pairwise files to block wise LAS files
#

B=3
THREADS=8
MEM=30

reads.singlelas:
        for reads.${i}.singlelas in 1 .. ${B}:
                LASLIST=reads.${i}.laslist
                @rm -f ${LASLIST}
                for reads.${i}.${j}.las in ${i} .. ${B}:
                        daligner -T${THREADS} -M${MEM} reads.${i} reads.${j}
                for reads.${i}.${j}.laslist in 1 .. ${B}:
                        @echo reads.${i}.${j}.las >> ${LASLIST}

for reads.${i}.las in 1 .. ${B}: reads.singlelas
        LASLIST=reads.${i}.laslist
        lasmerge2 -t${THREADS} -l reads.${i}.las ${LASLIST}

for reads.${i}.lasmerge2.cleanup in 1 .. ${B}: reads.${i}.las
        LASLIST=reads.${i}.laslist
        rm -f ${LASLIST}
```

daparse turns this into

```
#
# sample dapend file showing daligner dependencies
#
# we first produce all pairwise block alignments
# then we merge the pairwise files to block wise LAS files
#
reads.1.1.las: 
	daligner -T8 -M30 reads.1 reads.1
reads.1.2.las: 
	daligner -T8 -M30 reads.1 reads.2
reads.1.3.las: 
	daligner -T8 -M30 reads.1 reads.3
reads.1.1.laslist: 
#echo reads.1.1.las >> reads.1.laslist
reads.1.2.laslist: 
#echo reads.1.2.las >> reads.1.laslist
reads.1.3.laslist: 
#echo reads.1.3.las >> reads.1.laslist
reads.1.singlelas: reads.1.1.las reads.1.2.las reads.1.3.las reads.1.1.laslist reads.1.2.laslist reads.1.3.laslist
#rm -f reads.1.laslist
reads.2.2.las: 
	daligner -T8 -M30 reads.2 reads.2
reads.2.3.las: 
	daligner -T8 -M30 reads.2 reads.3
reads.2.1.laslist: 
#echo reads.2.1.las >> reads.2.laslist
reads.2.2.laslist: 
#echo reads.2.2.las >> reads.2.laslist
reads.2.3.laslist: 
#echo reads.2.3.las >> reads.2.laslist
reads.2.singlelas: reads.2.2.las reads.2.3.las reads.2.1.laslist reads.2.2.laslist reads.2.3.laslist
#rm -f reads.2.laslist
reads.3.3.las: 
	daligner -T8 -M30 reads.3 reads.3
reads.3.1.laslist: 
#echo reads.3.1.las >> reads.3.laslist
reads.3.2.laslist: 
#echo reads.3.2.las >> reads.3.laslist
reads.3.3.laslist: 
#echo reads.3.3.las >> reads.3.laslist
reads.3.singlelas: reads.3.3.las reads.3.1.laslist reads.3.2.laslist reads.3.3.laslist
#rm -f reads.3.laslist
reads.singlelas: reads.1.singlelas reads.2.singlelas reads.3.singlelas
reads.1.las: reads.singlelas
	lasmerge2 -t8 -l reads.1.las reads.1.laslist
reads.2.las: reads.singlelas
	lasmerge2 -t8 -l reads.2.las reads.2.laslist
reads.3.las: reads.singlelas
	lasmerge2 -t8 -l reads.3.las reads.3.laslist
reads.1.lasmerge2.cleanup: reads.1.las
	rm -f reads.1.laslist
reads.2.lasmerge2.cleanup: reads.2.las
	rm -f reads.2.laslist
reads.3.lasmerge2.cleanup: reads.3.las
	rm -f reads.3.laslist
```

On the way it produces the files reads.1.laslist, reads.2.laslist and
reads.3.laslist where the content of reads.1.laslist is

```
reads.1.1.las
reads.1.2.las
reads.1.3.las
```
