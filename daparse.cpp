/**
 * Copyright 2018-2019 German Tischler-Höhle
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products
 *    derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 **/
#include <string>
#include <iostream>
#include <vector>
#include <sstream>
#include <cassert>
#include <memory>
#include <map>

struct Token
{
	enum token_type
	{
		token_eof,
		token_colon,
		token_eq,
		token_at,
		token_sym
	};

	token_type type;
	char sym;

	Token() : type(token_eof), sym(0)
	{
	}
	Token(token_type const rtype)
	: type(rtype), sym(0)
	{

	}
	Token(token_type const rtype, char const rsym)
	: type(rtype), sym(rsym)
	{

	}
};

struct Tokeniser
{
	std::string const & s;
	std::string::const_iterator it_c;
	std::string::const_iterator it_e;

	Tokeniser(std::string const & rs)
	: s(rs), it_c(s.begin()), it_e(s.end())
	{

	}

	Token getNextToken()
	{
		if ( it_c == it_e )
			return Token(Token::token_eof);

		if ( *it_c == '\\' )
		{
			it_c++;

			if ( it_c == it_e )
			{
				return Token(Token::token_sym,'\\');
			}
			else
			{
				return Token(Token::token_sym,*it_c++);
			}
		}
		else
		{
			char const sym = *(it_c++);

			switch ( sym )
			{
				case '=':
					return Token(Token::token_eq);
				case ':':
					return Token(Token::token_colon);
				case '@':
					return Token(Token::token_at);
				default:
					return Token(Token::token_sym,sym);
			}
		}
	}
};

struct TokenVariable
{
	enum token_type
	{
		token_eof,
		token_dollar,
		token_lbrace,
		token_rbrace,
		token_sym
	};

	token_type type;
	char sym;

	TokenVariable() : type(token_eof), sym(0) {}
	TokenVariable(token_type const rtype) : type(rtype), sym(0) {}
	TokenVariable(token_type const rtype, char const rsym) : type(rtype), sym(rsym) {}
};

struct TokeniserVariable
{
	std::string const & s;
	std::string::const_iterator it_c;
	std::string::const_iterator it_e;

	TokeniserVariable(std::string const & rs)
	: s(rs), it_c(s.begin()), it_e(s.end())
	{

	}

	TokenVariable getNextToken()
	{
		if ( it_c == it_e )
			return TokenVariable(TokenVariable::token_eof);

		if ( *it_c == '\\' )
		{
			it_c++;

			if ( it_c == it_e )
			{
				return TokenVariable(TokenVariable::token_sym,'\\');
			}
			else
			{
				return TokenVariable(TokenVariable::token_sym,*it_c++);
			}
		}
		else
		{
			char const sym = *(it_c++);

			switch ( sym )
			{
				case '$':
					return TokenVariable(TokenVariable::token_dollar);
				case '{':
					return TokenVariable(TokenVariable::token_lbrace);
				case '}':
					return TokenVariable(TokenVariable::token_rbrace);
				default:
					return TokenVariable(TokenVariable::token_sym,sym);
			}
		}
	}

	static std::string replace(std::string const & s, std::map<std::string,std::string> const & M)
	{
		TokeniserVariable TOK(s);
		TokenVariable token;
		TokenVariable tokenprev;
		bool varopen = false;
		std::string::const_iterator vstart;
		std::string::const_iterator vend;
		std::vector< std::pair<uint64_t,uint64_t> > V;

		while ( (token=TOK.getNextToken()).type != TokenVariable::token_eof )
		{
			switch ( token.type )
			{
				case TokenVariable::token_dollar:
				{
					break;
				}
				case TokenVariable::token_lbrace:
				{
					if ( tokenprev.type == TokenVariable::token_dollar )
					{
						varopen = true;
						vstart = TOK.it_c-2;
					}
					break;
				}
				case TokenVariable::token_rbrace:
				{
					if ( varopen )
					{
						vend = TOK.it_c;
						varopen = false;

						V.push_back(
							std::pair<uint64_t,uint64_t>(
								vstart - TOK.s.begin(),
								vend - TOK.s.begin()
							)
						);
					}
				}
				default:
				{
					break;
				}
			}

			tokenprev = token;
		}

		if ( ! V.size() )
		{
			return s;
		}
		else
		{
			uint64_t prevend = 0;
			std::ostringstream str;

			for ( uint64_t i = 0; i < V.size(); ++i )
			{
				str << s.substr(prevend,V[i].first - prevend);

				std::string const varname = s.substr(
					V[i].first+2,
					V[i].second-V[i].first-3
				);

				std::map<std::string,std::string>::const_iterator it = M.find(varname);

				if ( it == M.end() )
				{
					std::cerr << "[E] replace: undefined variable " << varname << std::endl;
					throw std::runtime_error("[E] replace: undefined variable");
				}
				else
				{
					str << it->second;
				}

				prevend = V[i].second;
			}

			if ( prevend != s.size() )
			{
				str << s.substr(prevend);
			}

			return str.str();
		}
	}

	static std::string replacePart(std::string const & s, std::map<std::string,std::string> const & M)
	{
		TokeniserVariable TOK(s);
		TokenVariable token;
		TokenVariable tokenprev;
		bool varopen = false;
		std::string::const_iterator vstart;
		std::string::const_iterator vend;
		std::vector< std::pair<uint64_t,uint64_t> > V;

		while ( (token=TOK.getNextToken()).type != TokenVariable::token_eof )
		{
			switch ( token.type )
			{
				case TokenVariable::token_dollar:
				{
					break;
				}
				case TokenVariable::token_lbrace:
				{
					if ( tokenprev.type == TokenVariable::token_dollar )
					{
						varopen = true;
						vstart = TOK.it_c-2;
					}
					break;
				}
				case TokenVariable::token_rbrace:
				{
					if ( varopen )
					{
						vend = TOK.it_c;
						varopen = false;

						V.push_back(
							std::pair<uint64_t,uint64_t>(
								vstart - TOK.s.begin(),
								vend - TOK.s.begin()
							)
						);
					}
				}
				default:
				{
					break;
				}
			}

			tokenprev = token;
		}

		if ( ! V.size() )
		{
			return s;
		}
		else
		{
			uint64_t prevend = 0;
			std::ostringstream str;

			for ( uint64_t i = 0; i < V.size(); ++i )
			{
				str << s.substr(prevend,V[i].first - prevend);

				std::string const varname = s.substr(
					V[i].first+2,
					V[i].second-V[i].first-3
				);

				std::map<std::string,std::string>::const_iterator it = M.find(varname);

				if ( it == M.end() )
				{
					str << "${" << varname << "}";
				}
				else
				{
					str << it->second;
				}

				prevend = V[i].second;
			}

			if ( prevend != s.size() )
			{
				str << s.substr(prevend);
			}

			return str.str();
		}
	}

	static std::string getLastVariableName(std::string const & s)
	{
		TokeniserVariable TOK(s);
		TokenVariable token;
		TokenVariable tokenprev;
		bool varopen = false;
		std::string::const_iterator vstart;
		std::string::const_iterator vend;
		std::vector< std::pair<uint64_t,uint64_t> > V;

		while ( (token=TOK.getNextToken()).type != TokenVariable::token_eof )
		{
			switch ( token.type )
			{
				case TokenVariable::token_dollar:
				{
					break;
				}
				case TokenVariable::token_lbrace:
				{
					if ( tokenprev.type == TokenVariable::token_dollar )
					{
						varopen = true;
						vstart = TOK.it_c-2;
					}
					break;
				}
				case TokenVariable::token_rbrace:
				{
					if ( varopen )
					{
						vend = TOK.it_c;
						varopen = false;

						V.push_back(
							std::pair<uint64_t,uint64_t>(
								vstart - TOK.s.begin(),
								vend - TOK.s.begin()
							)
						);
					}
				}
				default:
				{
					break;
				}
			}

			tokenprev = token;
		}

		if ( V.size() )
			return s.substr(
				V.back().first+2,
				V.back().second-V.back().first-3
			);
		else
		{
			std::cerr << "[E] unable to find variable name in " << s << std::endl;
			throw std::runtime_error("[E] unable to find variable name");
		}
	}
};

struct Operation;
struct OperationVector;

struct Operation
{
	typedef Operation this_type;
	typedef std::shared_ptr<this_type> shared_ptr_type;

	Operation()
	{

	}
	virtual ~Operation()
	{

	}

	static std::string indent(unsigned int const d)
	{
		return std::string(d,'\t');
	}

	virtual shared_ptr_type clone() const = 0;

	virtual std::ostream & print(std::ostream & out, unsigned int const ind) const = 0;

	virtual void unroll(
		OperationVector & V,
		std::map<std::string,std::string> & M
	) const = 0;

	virtual void filldependencies() = 0;

	virtual std::ostream & printmk(std::ostream & out) const = 0;
};

struct OperationVector : public Operation
{
	typedef OperationVector this_type;
	typedef std::shared_ptr<this_type> shared_ptr_type;

	std::vector<Operation::shared_ptr_type> V;

	OperationVector() {}

	virtual std::ostream & print(std::ostream & out, unsigned int const ind = 0) const
	{
		for ( uint64_t i = 0; i < V.size(); ++i )
			V[i]->print(out,ind);
		return out;
	}

	virtual Operation::shared_ptr_type clone() const
	{
		shared_ptr_type tptr(new this_type);
		for ( uint64_t i = 0; i < V.size(); ++i )
			tptr->V.push_back(V[i]->clone());
		return tptr;
	}

	virtual void unroll(
		OperationVector & V,
		std::map<std::string,std::string> & M
	) const
	{
		for ( uint64_t i = 0; i < this->V.size(); ++i )
			this->V[i]->unroll(V,M);
	}

	virtual void filldependencies()
	{
		for ( uint64_t i = 0; i < V.size(); ++i )
		{
			V[i]->filldependencies();
		}
	}

	virtual std::ostream & printmk(std::ostream & out) const
	{
		for ( uint64_t i = 0; i < V.size(); ++i )
		{
			V[i]->printmk(out);
		}
		return out;
	}
};

struct OperationAssignment : public Operation
{
	typedef OperationAssignment this_type;
	typedef std::shared_ptr<this_type> shared_ptr_type;

	std::string key;
	std::string value;

	OperationAssignment() {}
	OperationAssignment(std::string const rkey, std::string const rvalue) : key(rkey), value(rvalue) {}

	virtual std::ostream & print(std::ostream & out, unsigned int const ind) const
	{
		out << indent(ind) << "OperationAssignment(" << key << "," << value << ")\n";
		return out;
	}

	virtual Operation::shared_ptr_type clone() const
	{
		shared_ptr_type tptr(new this_type(key,value));
		return tptr;
	}

	virtual void unroll(
		OperationVector & /* V */,
		std::map<std::string,std::string> & M
	) const
	{
		std::string const nkey = TokeniserVariable::replace(key,M);
		std::string const nvalue = TokeniserVariable::replace(value,M);

		M[nkey] = nvalue;
	}

	virtual void filldependencies() {}

	virtual std::ostream & printmk(std::ostream & out) const
	{
		return out;
	}
};

struct OperationComment : public Operation
{
	typedef OperationComment this_type;
	typedef std::shared_ptr<this_type> shared_ptr_type;

	std::string value;

	OperationComment() {}
	OperationComment(std::string const rvalue) : value(rvalue) {}

	virtual std::ostream & print(std::ostream & out, unsigned int const ind) const
	{
		out << indent(ind) << "OperationComment(" << value << ")\n";
		return out;
	}

	virtual Operation::shared_ptr_type clone() const
	{
		shared_ptr_type tptr(new this_type(value));
		return tptr;
	}

	virtual void unroll(
		OperationVector & V,
		std::map<std::string,std::string> & M
	) const
	{
		std::string const nvalue = TokeniserVariable::replace(value,M);

		Operation::shared_ptr_type tptr(new this_type(nvalue));

		V.V.push_back(tptr);
	}

	virtual void filldependencies() {}

	virtual std::ostream & printmk(std::ostream & out) const
	{
		out << "#" << value << "\n";
		return out;
	}
};

struct OperationCommand : public Operation
{
	typedef OperationCommand this_type;
	typedef std::shared_ptr<this_type> shared_ptr_type;

	std::string value;

	OperationCommand() {}
	OperationCommand(std::string const rvalue) : value(rvalue) {}

	virtual std::ostream & print(std::ostream & out, unsigned int const ind) const
	{
		out << indent(ind) << "OperationCommand(" << value << ")\n";
		return out;
	}

	virtual Operation::shared_ptr_type clone() const
	{
		shared_ptr_type tptr(new this_type(value));
		return tptr;
	}

	virtual void unroll(
		OperationVector & V,
		std::map<std::string,std::string> & M
	) const
	{
		std::string const nvalue = TokeniserVariable::replace(value,M);

		Operation::shared_ptr_type tptr(new this_type(nvalue));

		V.V.push_back(tptr);
	}

	virtual void filldependencies() {}

	virtual std::ostream & printmk(std::ostream & out) const
	{
		out << "\t" << value << "\n";
		return out;
	}
};

struct OperationInline : public Operation
{
	typedef OperationInline this_type;
	typedef std::shared_ptr<this_type> shared_ptr_type;

	std::string value;

	OperationInline() {}
	OperationInline(std::string const rvalue) : value(rvalue) {}

	virtual std::ostream & print(std::ostream & out, unsigned int const ind) const
	{
		out << indent(ind) << "OperationInline(" << value << ")\n";
		return out;
	}

	virtual Operation::shared_ptr_type clone() const
	{
		shared_ptr_type tptr(new this_type(value));
		return tptr;
	}

	virtual void unroll(
		OperationVector & V,
		std::map<std::string,std::string> & M
	) const
	{
		std::string const nvalue = TokeniserVariable::replace(value,M);

		Operation::shared_ptr_type tptr(new this_type(nvalue));

		char const * ccommand = nvalue.c_str();

		int const r = system(ccommand);

		if ( r != 0 )
		{
			std::cerr << "[E] failed to run " << nvalue << std::endl;
			throw std::runtime_error("[E] failed to run inline command");
		}

		V.V.push_back(tptr);
	}

	virtual void filldependencies() {}

	virtual std::ostream & printmk(std::ostream & out) const
	{
		out << "#" << value << "\n";
		return out;
	}
};

struct OperationRule : public Operation
{
	typedef OperationRule this_type;
	typedef std::shared_ptr<this_type> shared_ptr_type;

	std::string target;
	std::string dependencies;
	OperationVector::shared_ptr_type ops;

	std::string childtargets;

	bool containsRules() const
	{
		for ( uint64_t i = 0; i < ops->V.size(); ++i )
		{
			Operation const * op = ops->V[i].get();

			if ( dynamic_cast<OperationRule const *>(op) != NULL )
				return true;
		}

		return false;
	}

	std::string getSubRuleTargets() const
	{
		std::ostringstream targetstr;

		for ( uint64_t i = 0; i < ops->V.size(); ++i )
		{
			Operation const * op = ops->V[i].get();

			if ( dynamic_cast<OperationRule const *>(op) != NULL )
			{
				OperationRule const & rule = *(dynamic_cast<OperationRule const *>(op));

				targetstr << " " << rule.target;
			}
		}

		return targetstr.str();
	}

	static std::vector<std::string> wsclip(std::vector<std::string> V)
	{
		for ( uint64_t i = 0; i < V.size(); ++i )
			V[i] = wsclip(V[i]);

		return V;
	}

	static std::string wsclip(std::string s)
	{
		uint64_t f = 0;
		while ( f < s.size() && isspace(s[f]) )
			++f;
		s = s.substr(f);

		uint64_t b = 0;
		while ( b < s.size() && isspace(s[s.size()-b-1]) )
			++b;

		s = s.substr(0,s.size()-b);

		return s;
	}

	OperationRule() {}
	OperationRule(
		std::string const & rtarget,
		std::string const & rdependencies,
		OperationVector::shared_ptr_type rops
	) : target(rtarget), dependencies(wsclip(rdependencies)), ops(rops)
	{

	}

	bool isTargetMulti() const
	{
		return
			(target.size() >= 4) && target.substr(0,4) == "for ";
	}

	bool isDepMulti() const
	{
		return
			(dependencies.size() >= 4) && dependencies.substr(0,4) == "for ";
	}

	virtual std::ostream & print(std::ostream & out, unsigned int const ind) const
	{
		out << indent(ind) << "OperationRule" << "(";

		out << "[" << target << "]";

		out << ",[" << dependencies << "])\n";

		for ( uint64_t i = 0; i < ops->V.size(); ++i )
			ops->V[i]->print(out,ind+1);

		return out;
	}

	virtual Operation::shared_ptr_type clone() const
	{
		OperationVector::shared_ptr_type tops(new OperationVector);
		for ( uint64_t i = 0; i < ops->V.size(); ++i )
		{
			Operation::shared_ptr_type tptr(ops->V[i]->clone());
			tops->V.push_back(tptr);
		}
		shared_ptr_type tptr(new this_type(target,dependencies,tops));
		return tptr;
	}

	static uint64_t parseNumber(std::string const & s)
	{
		std::istringstream istr(s);
		uint64_t u;

		istr >> u;

		if ( ! istr || istr.peek() != std::istream::traits_type::eof() )
		{
			std::cerr << "[E] unable to parse number " << s << std::endl;
			throw std::runtime_error("[E] unable to parse number");
		}

		return u;
	}

	struct ForSubst
	{
		std::string entry;
		std::string name;
		std::string value;

		ForSubst() {}
		ForSubst(
			std::string const & rentry,
			std::string const & rname,
			std::string const & rvalue
		) : entry(rentry), name(rname), value(rvalue)
		{

		}
	};

	static std::vector<ForSubst> unrollFor(
		std::string const & s,
		std::map<std::string,std::string> const & M
	)
	{
		std::vector<std::string> Q;

		uint64_t i = 0;
		while ( i < s.size() )
		{
			while ( i < s.size() && isspace(s[i]) )
				++i;

			uint64_t j = i;
			while ( j < s.size() && !isspace(s[j]) )
				++j;

			if ( j > i )
				Q.push_back(
					s.substr(i,j-i)
				);

			i = j;
		}

		// for reads.${i} in 1 .. ${B}

		if (
			Q.size() < 6
			||
			Q[0] != "for"
			||
			Q[2] != "in"
			||
			Q[4] != ".."
		)
		{
			std::cerr << "[E] unparsable for loop " << s << std::endl;
			throw std::runtime_error("[E] unparsable for loop");
		}

		std::string const varname = TokeniserVariable::getLastVariableName(Q[1]);

		Q[1] = TokeniserVariable::replacePart(Q[1],M);
		Q[3] = TokeniserVariable::replacePart(Q[3],M);
		Q[5] = TokeniserVariable::replacePart(Q[5],M);

		uint64_t const ufrom = parseNumber(Q[3]);
		uint64_t const uto = parseNumber(Q[5]);

		std::vector<ForSubst> VO;

		for ( uint64_t u = ufrom; u <= uto; ++u )
		{
			std::ostringstream ostr;
			ostr << u;
			std::map<std::string,std::string> ML;
			ML[varname] = ostr.str();

			std::string const so = TokeniserVariable::replace(Q[1],ML);
			// VO.push_back(so);

			ForSubst FS(so,varname,ostr.str());

			VO.push_back(FS);
		}

		return VO;
	}

	virtual void unroll(
		OperationVector & V,
		std::map<std::string,std::string> & M
	) const
	{
		if ( isTargetMulti() )
		{
			std::vector<ForSubst> targV = unrollFor(
				target,
				M
			);

			for ( uint64_t i = 0; i < targV.size(); ++i )
			{
				ForSubst const & FS = targV[i];
				std::map<std::string,std::string> MS = M;
				MS[FS.name] = FS.value;

				std::string const ntarget = FS.entry;
				std::ostringstream ndepstr;

				if ( isDepMulti() )
				{
					std::vector<ForSubst> const VD = unrollFor(dependencies,MS);

					for ( uint64_t i = 0; i < VD.size(); ++i )
					{
						if ( i )
							ndepstr << " ";
						ndepstr << VD[i].entry;
					}
				}
				else
				{
					ndepstr << TokeniserVariable::replace(dependencies,MS);
				}

				OperationVector::shared_ptr_type subV(
					new OperationVector
				);

				for ( uint64_t i = 0; i < ops->V.size(); ++i )
					ops->V[i]->unroll(*subV,MS);

				OperationRule::shared_ptr_type tptr(
					new OperationRule(
						ntarget,
						ndepstr.str(),
						subV
					)
				);

				V.V.push_back(tptr);
			}
		}
		else
		{
			std::map<std::string,std::string> MS = M;

			std::string const ntarget = TokeniserVariable::replace(target,MS);
			std::ostringstream ndepstr;

			if ( isDepMulti() )
			{
				std::vector<ForSubst> const VD = unrollFor(dependencies,MS);

				for ( uint64_t i = 0; i < VD.size(); ++i )
				{
					if ( i )
						ndepstr << " ";
					ndepstr << VD[i].entry;
				}
			}
			else
			{
				ndepstr << dependencies;
			}

			OperationVector::shared_ptr_type subV(
				new OperationVector
			);

			for ( uint64_t i = 0; i < ops->V.size(); ++i )
				ops->V[i]->unroll(*subV,MS);

			OperationRule::shared_ptr_type tptr(
				new OperationRule(
					ntarget,
					ndepstr.str(),
					subV
				)
			);

			V.V.push_back(tptr);
		}
	}

	virtual void filldependencies()
	{
		for ( uint64_t z = 0; z < ops->V.size(); ++z )
		{
			Operation * op = ops->V[z].get();

			if ( dynamic_cast<OperationRule *>(op) != NULL )
			{
				OperationRule & OPR = *dynamic_cast<OperationRule *>(op);

				OPR.filldependencies();

				dependencies += " " + OPR.target;
			}
		}

		dependencies = wsclip(dependencies);
	}

	virtual std::ostream & printmk(std::ostream & out) const
	{
		for ( uint64_t z = 0; z < ops->V.size(); ++z )
		{
			Operation const * op = ops->V[z].get();

			if ( dynamic_cast<OperationRule const *>(op) != NULL )
				op->printmk(out);
		}

		out << target << ": " << dependencies << "\n";

		for ( uint64_t z = 0; z < ops->V.size(); ++z )
		{
			Operation const * op = ops->V[z].get();

			if ( dynamic_cast<OperationRule const *>(op) == NULL )
				op->printmk(out);
		}

		return out;
	}
};

OperationVector::shared_ptr_type parse(std::istream & in, unsigned int level = 0)
{
	try
	{
		OperationVector::shared_ptr_type OP(
			new OperationVector
		);

		std::vector<std::string> VL;

		std::string line;

		while ( std::getline(in,line) )
		{
			if ( line.size() && line[0] != '\t' )
			{
				uint64_t clip = 0;

				while ( clip < line.size() && isspace(line[line.size()-clip-1]) )
					++clip;

				line = line.substr(0,line.size()-clip);
			}

			VL.push_back(line);
		}

		uint64_t ilow = 0;

		while ( ilow < VL.size() )
		{
			Tokeniser TOK(VL[ilow]);
			Token token;

			enum line_type
			{
				line_type_assignment,
				line_type_inline,
				line_type_rule,
				line_type_comment,
				line_type_none
			};

			std::string const & line = VL[ilow];
			line_type lt = line_type_none;

			if ( line.size() && line[0] == '@' )
				lt = line_type_inline;
			else if ( line.size() && line[0] == '#' )
				lt = line_type_comment;
			else if ( line.size() && line[0] == '\t' )
			{
				// std::cerr << "[E] line number " << ilow << " line " << line << " starts with tab outside a rule" << std::endl;
				throw std::runtime_error("[E] tab outside rule");
			}
			else if ( line.size() )
			{
				while (
					lt == line_type_none
					&&
					(token=TOK.getNextToken()).type != Token::token_eof
				)
				{
					switch ( token.type )
					{
						case Token::token_colon:
							lt = line_type_rule;
							break;
						case Token::token_eq:
							lt = line_type_assignment;
							break;
						default:
							break;
					}
				}

				if ( lt == line_type_none )
				{
					if ( level )
					{
						OperationCommand::shared_ptr_type tOP(
							new OperationCommand(line)
						);

						OP->V.push_back(tOP);
					}
					else
					{
						std::cerr << "[E] line number " << ilow << " line " << line << " unknown line type" << std::endl;
						throw std::runtime_error("[E] unable to parse line");
					}
				}
			}

			switch ( lt )
			{
				case line_type_none:
				{
					ilow += 1;
					break;
				}
				case line_type_assignment:
				{
					Tokeniser TOK(VL[ilow++]);

					std::ostringstream namestr;

					while (
						(token=TOK.getNextToken()).type != Token::token_eq
					)
					{
						switch ( token.type )
						{
							case Token::token_at:
								namestr.put('@');
								break;
							case Token::token_sym:
								namestr.put(token.sym);
								break;
							case Token::token_eof:
							case Token::token_colon:
							case Token::token_eq:
							default:
								assert ( false );
								break;
						}
					}

					std::string const k(namestr.str());
					std::string const v(TOK.it_c,TOK.it_e);

					// std::cerr << "key=" << k << " v=" << v << std::endl;

					OperationAssignment::shared_ptr_type LOP(new OperationAssignment(k,v));

					OP->V.push_back(LOP);

					break;
				}
				case line_type_inline:
				{
					std::string line = VL[ilow++].substr(1);

					OperationInline::shared_ptr_type LOP(new OperationInline(line));

					OP->V.push_back(LOP);

					break;
				}
				case line_type_comment:
				{
					std::string line = VL[ilow++].substr(1);

					OperationComment::shared_ptr_type LOP(new OperationComment(line));

					OP->V.push_back(LOP);

					break;
				}
				case line_type_rule:
				{
					Tokeniser TOK(VL[ilow++]);

					std::ostringstream namestr;

					while (
						(token=TOK.getNextToken()).type != Token::token_colon
					)
					{
						switch ( token.type )
						{
							case Token::token_at:
								namestr.put('@');
								break;
							case Token::token_sym:
								namestr.put(token.sym);
								break;
							case Token::token_eof:
							case Token::token_colon:
							case Token::token_eq:
							default:
								assert ( false );
								break;
						}
					}

					std::string const k(namestr.str());
					std::string const v(TOK.it_c,TOK.it_e);

					uint64_t ihigh = ilow;

					std::ostringstream substr;

					while ( ihigh < VL.size() && VL[ihigh].size() && VL[ihigh][0] == '\t' )
						substr << VL[ihigh++].substr(1) << "\n";

					std::istringstream subistr(substr.str());

					OperationVector::shared_ptr_type subptr(parse(subistr,level+1));

					OperationRule::shared_ptr_type opptr
					(
						new OperationRule
						(
							k,v,subptr
						)
					);

					OP->V.push_back(opptr);

					ilow = ihigh;

					break;
				}
			}
		}

		return OP;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		throw;
	}
}

int main()
{
	try
	{
		// parse the input file
		OperationVector::shared_ptr_type OP = parse(std::cin);

		// unroll all for loops
		OperationVector::shared_ptr_type NOP(new OperationVector);
		std::map<std::string,std::string> M;
		OP->unroll(*NOP,M);

		// fill in dependencies for nested for loops
		NOP->filldependencies();

		// print internal representation on stderr channel
		NOP->print(std::cerr);

		// print make file on standard output
		NOP->printmk(std::cout);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
